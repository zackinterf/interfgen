package _2022_04.rao;

import java.io.File;

import _0001.file.er.r.ReadErForFile;
import gen.language.en.verb._1.p.Process_enI;

public class ProcessErUnicodeDirToAdd implements Process_enI {
ReadErForFile  rff= new ReadErForFile();
String cDirDelim ="\\\\";
private String cApo="\"";

public static void main( String args[]){
	new ProcessErUnicodeDirToAdd().process();
}
@Override
public Object process(Object... oa) {
	 rff.setFileName("D:\\workspace\\interfgen-master\\unicode.txt");
	 rff.setProcessO1_enI( new LineProcessor() );
	 rff.init();
	 rff.execute();
	 rff.finish();
	return null;
}
private class LineProcessor implements Process_enI {

	private boolean log=false;
	String code;
	String name;
	
	@Override
	public Object process(Object... oa) {
		String dir = (String) oa[0];
		if (log) System.out.println(dir);
		String sa[] = dir.split(cDirDelim);
		name=sa[sa.length-2];
		if (log) System.out.println("name:"+name);
	    File[] fileList = new File(dir).listFiles();
	    
	    for (File file: fileList) { 
	    	if (!file.getName().equals(".gitkeep")) {

	    		code=file.getName();
				if (log) System.out.println("code:"+code);
	    	}
	    }
	    System.out.println("r.add("+cApo+name+cApo+","+cApo+code+cApo+");");
		return null;
	}
}

}
