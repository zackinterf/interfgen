package _2022_04.rao;

import java.io.File;
/*
import org.zackify.en._000.interf.f.Finish_enI;
import org.zackify.en._000.interf.i.Init_enI;

import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;
*/

//import org.zackify.en._000.interf.p.InitProcFinish_enI;

import _2022_04.rao.*;
import _2022_04.rao.basic.VectorEr;
import _2022_04.rao.proc.ProcessErForDebug;
import _2022_04.rao.proc.ProcessErForGraphViz;
import _2022_04.rao.proc.interf.ProcessErForHtml_OT_Unicode;
import _2022_04.rao.proc.interf.ProcessErForInterface;
import _2022_04.rao.proc.interf.ProcessErForSetEr;
import _2022_04.rao.proc.interf.ProcessorForExtendEr;
import gen.language.en.other.u.Unicode_enI;
import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;
import _0001.file.er.p.PrintErForFile;

public class GenerateEr_01  
extends ProcessErForFolder
//implements gen.language.en_2.verb._1.f.Finish_enI
{
	boolean codeGen=//false;
			true;

	String mode="";
	int level=0;
	String cSpecialPrefix="__";
	public String cInterfPostfix ="_enI";
	public String cPublic="public ";
	public String cPackage="package ";
	public String cInterface="interface ";
	public String cBrack_OT_Open ="(";
	public String cBrack_OT_Close =")";
	public String cBrack_OT_Open_OT_curly ="{";
	public String cBrack_OT_Close_OT_curly ="}";
	public String cSpace =" ";
	public String cDot =".";
	public String cApo ="\"";
	public String cSemicolon =";";
	String cTag="tag";
	public String cString="String";
	public String javaExt=".java";
	public String cGen="gen";
	public String cInt="int";
	String cUnderScore="_";
    String cDirDelim ="\\\\";
	public String cPathDelim = "\\";
    String cNoAlfa="noAlfa";
	public PrintErForFile pff = new PrintErForFile();
	public String cVisit_enI="Visit_enI";

	
	VectorEr vectorEr = null;
//	VectorEr vectorEr= new VectorEr();
	
	public static void main( String args[]){
		//System.out.println("test");
		new GenerateEr_01().visit();
	}

	public String genPath="D:\\workspace\\interfgen-master\\java\\src\\";
	//public String genPath="D:\\workspace\\test_java\\src\\";
	InitProcFinish_enI initProcFinish_enI=this;
	InitProcFinish_enI initProcFinishForInterface_enI=null;
	InitProcFinish_enI initProcFinishForSetEr_enI=null;
	InitProcFinish_enI initProcFinishForExtendEr_enI=null;
	InitProcFinish_enI initProcFinishForHtml_OT_Unicode=null;
	// =================================
	public String derivePackage(String path) {
		path = path.replace(File.separator,".");
		if (path.startsWith(cDot)) { path=path.substring(1); }
		return  path;
	}
	@Override
	public Object visit(Object ... oa) {
		System.out.println("visit!");
	
		String fullPath= genPath+cGen+cPathDelim;
		// initProcFinish_enI can be set to different values
		//initProcFinish_enI = new ProcessErForDebug();
		//codeGen=false;
		//initProcFinish_enI = new ProcessErForGraphViz();
		initProcFinishForInterface_enI = (InitProcFinish_enI) new ProcessErForInterface().setGenerateEr_01(this);
		initProcFinishForSetEr_enI = (InitProcFinish_enI) new ProcessErForSetEr().setGenerateEr_01(this);
		initProcFinishForExtendEr_enI = (InitProcFinish_enI) new ProcessorForExtendEr().setGenerateEr_01(this);
		initProcFinishForHtml_OT_Unicode = (InitProcFinish_enI) new ProcessErForHtml_OT_Unicode().setGenerateEr_01(this);
		
		if (initProcFinish_enI!=null) { ((Init_enI) initProcFinish_enI).init(); }
		if (codeGen) {
		  System.out.println(fullPath);
		  System.out.println(deleteDirectory(new File(fullPath)));
		  new File(fullPath).mkdir();
		}
		super.visit();
		if (initProcFinish_enI!=null) { ((Finish_enI)initProcFinish_enI).finish(); }

		return null;
	}

	//=================================
	boolean deleteDirectory(File directoryToBeDeleted) {
		File[] allContents = directoryToBeDeleted.listFiles();
		if (allContents != null) {
			for (File file : allContents) {
				deleteDirectory(file);
			}
		}
		return directoryToBeDeleted.delete();
	}

	// =================================
	@Override
	public Object process(Object ... oa) {
		String dir=oa[0].toString();
		String s=oa[1].toString();
		//System.out.println("generate process::"+dir+ " "+s);
		
		//System.out.println("generate process2:"+dir);
		if (dir.equals("dir")) { dir(s); }
		return this;
	}
	// =================================
	private void dir(String input) {
		System.out.println("dir::"+input);
		String [] stringArray;
		String s=input.replaceAll(" ", "_");
		stringArray=s.split(cDirDelim );
		/*
		System.out.println("dirMethod:"+stringArray.length);
		if (stringArray.length==2) {
			System.out.println("create directory ...");
			String pathName=genPath+cGen+cPathDelim+stringArray[1];
			createDir(pathName);
		}
		else {
		*/
		  if (stringArray.length>1) {
			  String last = stringArray[stringArray.length-1];
			  prepareSplitt(last);
			  String pathName=  concatAllButLast(stringArray);
	  	      if (input.endsWith("__"+Unicode_enI.t) ) {
	  	    	  System.out.print("!!name: "+pathName+" last:"+last);
	  	      }
			  //System.out.println("pathName:"+pathName);
			  //super.debug
			  if (true) {System.out.println(pathName+ " >>"+ last);}
			  if (codeGen) {
				  createDir(pathName);

			      if (noNumber(last) && noAlfa(pathName,last) && noTwoUnderScore(last) ) { 
			    	  if (initProcFinishForInterface_enI!=null) {
			    		  //System.out.print("!!process interfaces");
			    		  ((Process_enI)initProcFinishForInterface_enI).process(pathName,last);
  			    	      ((Process_enI) initProcFinishForSetEr_enI).process(pathName,last);
  			    	      ((Process_enI)initProcFinishForExtendEr_enI).process(pathName,last);
			    	  }
			      }
			  } else {
			  if (initProcFinish_enI !=null) {
				  ((Process_enI)initProcFinish_enI).process(pathName,last); 
				  }
			  }
		  }
		  
	}
	private void uni(String pathName ,String last) {
		  String myName=rootPath+ pathName+"\\__"+Unicode_enI.t;
  	    System.out.print("name: "+myName+" last:"+last);
  	      if (new File (myName).exists() ) {
  	    	  System.out.print("!!name: "+myName+" last:"+last);
  	      }
	}
	// handle strings like "entity_OT_artificial" as entity & artificial 
	private void prepareSplitt(String s) {
		String[] sa = s.split("_");
	
		for (int i=0;i<sa.length;i++) {
			if (sa[i].equals("OT")) {
				sa[i]="";
			}
		}	
		for (int i=0;i<sa.length;i++) {
		  splittAndAdd(sa[i]);
		}
	}
	private Object splittAndAdd(String s) {
		s=replace_OT_mine("_",s);
		s=replace_OT_mine("_",s);
		String[] r = s.split("(?=\\p{Lu})");
		String up="";
		for (int i=0;i<r.length;i++) {
			if (i > 0) {
				// in combinations like "studiedBy" do not work with big letters
				r[i]=r[i].toLowerCase();
				if (isStringUpperCase(r[i - 1])) {
					r[i - 1] = r[i - 1] + r[i];
					r[i] = "";
				}
			}
		}	
		for (int j=0;j<r.length;j++) {
		  if (vectorEr!=null) {vectorEr.add(r[j]);}
		}
		return null;
	}
	private String replace_OT_mine(String prefix,String s) {
		if (s.startsWith(prefix)) {s=s.substring(0,prefix.length());}
		return s;
	}
private  boolean isStringUpperCase(String str){
        
        //convert String to char array
        char[] charArray = str.toCharArray();
        
        for(int i=0; i < charArray.length; i++){
            
            //if the character is a letter
            if( Character.isLetter(charArray[i]) ){
                
                //if any character is not in upper case, return false
                if( !Character.isUpperCase( charArray[i] ))
                    return false;
            }
        }
        
        return true;
    }
	// =================================
	private boolean noTwoUnderScore(String name) {
		// TODO Auto-generated method stub
		return ! name.startsWith(cUnderScore+cUnderScore);
	}
	// =================================
	private void createDir(String dir) {
		File fi = new File(genPath+cGen+cPathDelim+dir);
		if (!fi.exists()) {
		  //System.out.println("create directory ...");
		  fi.mkdir();
		}
	}
	public ProcessErForFolder super_() {
		return this;
	}
	// =================================
	private boolean noAlfa(String pathName,String last) {
		boolean value=true;
		// super.rootPath+
		if (last.length()==1) {
				           value= ! new File( super.rootPath+pathName+cUnderScore+cUnderScore+cNoAlfa).exists() ;
		}
		//super.rootPath
		// for testing ...
		//System.out.println("check:"+super.rootPath+pathName+cUnderScore+cUnderScore+cNoAlfa+" "+last);
		//if (value) System.out.println("NoAlfa exists!");
		return value;
	}
	// =================================
	private boolean noNumber(String last) {
		boolean value=true;
		if (last.substring(0, 1).equals(cUnderScore)) {
			value=! isNumeric(last.substring(1));
		}
		//System.out.println(last+"="+value+" "+isNumeric(last.substring(1))+" "+last.substring(1));
		return value;
	}
	// =================================
	public  boolean isNumeric(String str) {
		  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}
	// =================================
	private String concatAllButLast(String[] stringArray) {
		String allButLast="";
		for (int i=0;i<stringArray.length-1;i++) {
			allButLast=allButLast+stringArray[i]+cPathDelim;
		}
		return allButLast;
	}
	// =================================
	private void genInterface(String dir ,String name) {


	}

	// =================================
	public String firstUpper(String string) {
		if (Character.isUpperCase(string.substring(0,1).toCharArray()[0])) {
			string=string+"U";
		}
		return string.substring(0,1).toUpperCase()+string.substring(1); 
	}
	@Override
	public Object finish(Object... oa) {
		super.finish(oa);
		if (codeGen) 
			if (vectorEr!=null) { vectorEr.process_OT_all(); }
		return null;
	}
}
