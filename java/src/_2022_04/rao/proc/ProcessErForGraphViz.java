package _2022_04.rao.proc;
/*
import org.zackify.en._000.interf.f.Finish_enI;
import org.zackify.en._000.interf.i.Init_enI;
import org.zackify.en._000.interf.p.InitProcFinish_enI;
import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;
*/

import _0001.InitProcFinishEr;
import gen.language.en.verb._1.v.Visit_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;

public class ProcessErForGraphViz extends InitProcFinishEr

implements InitProcFinish_enI,Visit_enI
{

	private String apo = "\"";
	//String cPathDelim = "\\";
	String cDirDelim ="\\\\";
	String cDot =".";
	String currentDir="";
	int i =0;
	
	@Override
	public Visit_enI process(Object... oa) {
		String myDir = apo+"dir_"+filter(oa[0].toString());
		System.out.println( myDir+apo +" -> "+myDir+cDot+oa[1]+apo+";");
		myDir = myDir+apo;
		if (i!=0) {
			if (!currentDir.equals(myDir)) {
				if (currentDir.length() <myDir.length()) {
				  System.out.println( currentDir +" -> "+myDir);
				}
				currentDir = myDir;
			}
		} else currentDir = myDir;
		i++;
		return this;
	}

	private String filter(String s) {
		String name = s.replaceAll(cDirDelim,cDot);
		if (name.endsWith(cDot)) name=name.substring(0,name.length()-1);
		return name;
	}

	@Override
	public Object init(Object... oa) {
		System.out.println("digraph G {");
		return null;
	}

	@Override
	public Object finish(Object... oa) {
		System.out.println("}");
		return null;
	}

	@Override
	public Object visit(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

}
