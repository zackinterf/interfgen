package _2022_04.rao.proc.interf;



import _0001.InitProcFinishEr;
import _0001.file.er.p.PrintErForFile;
import _2022_04.rao.GenerateEr_01;
import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;

public class ProcessorForExtendEr  // extends InitProcFinishEr
implements InitProcFinish_enI,Init_enI,Finish_enI,Process_enI {
	GenerateEr_01 generateEr_01;
	int counter=0;
	PrintErForFile pff =  new PrintErForFile();
	
	@Override
	public Object init(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Object process2(Object... oa) {
		System.out.println("add:"+oa[0]+" - "+oa[1]);
		return null;
	}

	@Override
	public Object finish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}



	public InitProcFinish_enI setGenerateEr_01(GenerateEr_01 generateEr_01) {
		this.generateEr_01 = generateEr_01;
		return this;
	}


	@Override
	public Object process(Object... oa) {
		String dir =oa[0].toString();
		String name =oa[1].toString();
		String className=generateEr_01.firstUpper(name);
		String packagePath = generateEr_01.derivePackage(generateEr_01.cGen+dir);
		//isJavaKeyword = new File ("D:\\_2022\\_2022_04\\tag\\java\\keyword\\"+name).exists();
		//isJavaKeyword = isJavaKeyword || new File ("D:\\_2022\\_2022_04\\tag\\java\\datatype\\basic\\"+name).exists();
		packagePath= packagePath.substring(0, packagePath.length()-1);
		pff.setFileName(generateEr_01.genPath+
				        generateEr_01.cGen+
				        generateEr_01.cPathDelim+dir+"Set"+className+
				        generateEr_01.cInterfPostfix+
				        generateEr_01.javaExt);
		pff.init();
		pff.println(generateEr_01.cPackage+packagePath+generateEr_01.cSemicolon);
		//pff.println("import gen.language.en_2.verb._1.v.Visit_enI;");
		pff.println(generateEr_01.cPublic+generateEr_01.cInterface+generateEr_01.cSpace+""+"Set"+className+generateEr_01.cInterfPostfix+" extends "+className
				+generateEr_01.cInterfPostfix+generateEr_01.cSpace+generateEr_01.cBrack_OT_Open_OT_curly);
		//generateTag(packagePath,name);	
		//generateMethod(name,className);
		pff.println(generateEr_01.cBrack_OT_Close_OT_curly);
		pff.finish();
		counter++;
		return null;

	}


	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return this;
	}

}
