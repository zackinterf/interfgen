package _2022_04.rao.proc.interf;

import java.io.File;
/*
import org.zackify.en._000.interf.p.InitProcFinish_enI;
import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;
//import org.zackify.en.prep.er.p.PrintErForFile;
*/

import _0001.InitProcFinishEr;
import _0001.file.er.p.PrintErForFile;
import _0001.file.er.r.ReadErForFile;
import _2022_04.rao.GenerateEr_01;
import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;

public class ProcessErForInterface //extends InitProcFinishEr
implements InitProcFinish_enI,Init_enI,Process_enI,Finish_enI
{
	GenerateEr_01 generateEr_01=null;
	PrintErForFile pff =  new PrintErForFile();
	int counter=0;
	boolean isJavaKeyword=false;
	public String value="";
	// =================================
	public Object setGenerateEr_01(GenerateEr_01 generateEr_01) {
		this.generateEr_01 = generateEr_01;
		return this;
	}
	// =================================
	@Override
	public Object init(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}
	// =================================
	@Override
	public Object process(Object... oa) {
		if (generateEr_01.isUnicodeDir()) {return null;}
		String dir =oa[0].toString();
		String name =oa[1].toString();
		if (dir.contains("__unicode")) {return null;}
		if (name.contains("__unicode")) {return null;}
		String className=generateEr_01.firstUpper(name);
		String packagePath = generateEr_01.derivePackage(generateEr_01.cGen+dir);
		isJavaKeyword = new File (generateEr_01.rootPath+"\\java\\keyword\\"+name).exists();
		isJavaKeyword = isJavaKeyword || new File (generateEr_01.rootPath+"\\java\\datatype\\basic\\"+name).exists();
		packagePath= packagePath.substring(0, packagePath.length()-1);
		pff.setFileName(generateEr_01.genPath+
				        generateEr_01.cGen+
				        generateEr_01.cPathDelim+dir+className+
				        generateEr_01.cInterfPostfix+
				        generateEr_01.javaExt);
		pff.init();
		pff.println(generateEr_01.cPackage+packagePath+generateEr_01.cSemicolon);
		pff.println("import gen.language.en.verb._1.v.Visit_enI;");
		pff.println(generateEr_01.cPublic+generateEr_01.cInterface+generateEr_01.cSpace+className
				+generateEr_01.cInterfPostfix+generateEr_01.cSpace+generateEr_01.cBrack_OT_Open_OT_curly);
		generateTag(packagePath,name);	
		generateConstant(name,className);
		generateMethod(name,className);
		generateValue(dir,name);
		pff.println(generateEr_01.cBrack_OT_Close_OT_curly);
		pff.finish();
		counter++;
		return null;
	}
	private void generateValue(String dir, String name) {
		ReadErForFile rff =  new ReadErForFile();
		rff.setFileName(generateEr_01.super_().rootPath+dir+name+generateEr_01.cPathDelim+"val.txt");
		// pff.println("// "+rff.getFileName());
		if (new File (rff.getFileName()).exists()) {
		rff.setProcessO1_enI(new ProcessErForValue(this));
		rff.visit();
		pff.println("String v="+generateEr_01.cApo+value+generateEr_01.cApo+generateEr_01.cSemicolon);
		rff.finish();
		}
	}
	private void generateMethod(String name, String className) {
		String u="";
		String k="";
		if (isJavaKeyword) { k="_";}
		if ((name+"U").equals(className)) {u="U";}
		pff.println(generateEr_01.cPublic+"Object"+" "+name+u+k+" (Object ... oa) ;");
		//generateEr_01.cSpace+generateEr_01.cVisit_enI
	}
	private void generateConstant(String name,String className) {
		// TODO Auto-generated method stub
		pff.println("// "+generateEr_01.cString+generateEr_01.cSpace+"c"+className+
				" ="+generateEr_01.cApo+name+generateEr_01.cApo+generateEr_01.cSemicolon);
	}
	// =================================
	private void generateTag( String  packagePath, String name) {
		pff.println(generateEr_01.cString+generateEr_01.cSpace+
				"t ="+generateEr_01.cApo+name+generateEr_01.cApo+generateEr_01.cSemicolon);
		pff.println(generateEr_01.cString+generateEr_01.cSpace+
				"pack ="+generateEr_01.cApo+ packagePath+generateEr_01.cApo+generateEr_01.cSemicolon);
		pff.println(generateEr_01.cInt+generateEr_01.cSpace+
				"num = "+ counter+generateEr_01.cSemicolon);
		generateType(packagePath,name);
	}
	// =================================
	private void generateType(String packagePath, String name) {
		pff.println(generateEr_01.cString+generateEr_01.cSpace+
				"type ="+generateEr_01.cApo+deriveType(packagePath) +generateEr_01.cApo+generateEr_01.cSemicolon);
	}
	private String deriveType(String packagePath) {
		String type="";
		if (packagePath.contains("adject")) { type ="adjective";}
		return type;
	}

	// =================================
	@Override
	public Object finish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public class ProcessErForValue implements Process_enI {

		private ProcessErForInterface processorForInterface;

		public ProcessErForValue(ProcessErForInterface processorForInterface) {
			this.processorForInterface = processorForInterface;
		}

		@Override
		public Object process(Object... oa) {
			// TODO Auto-generated method stub
			return null;
		}



	}

	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return this;
	}
}
