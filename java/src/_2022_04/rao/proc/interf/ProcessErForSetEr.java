package _2022_04.rao.proc.interf;

import java.io.File;


import _0001.file.er.p.PrintErForFile;

/*
import org.zackify.en._000.interf.p.InitProcFinish_enI;
import org.zackify.en._000.interf.v.Visit_enI;
import org.zackify.en.prep.er.p.PrintErForFile;
*/

import _2022_04.rao.GenerateEr_01;
import gen.language.en.verb._1.p.Process_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;

public class ProcessErForSetEr // extends InitProcFinishEr
implements InitProcFinish_enI , Process_enI{
	int counter=0;
	PrintErForFile pff =  new PrintErForFile();
	private GenerateEr_01 generateEr_01;
	// =================================
	public Object setGenerateEr_01(GenerateEr_01 generateEr_01) {
		this.generateEr_01 = generateEr_01;
		return this;
	}
	// =================================
	@Override
	public Object process(Object... oa) {
		String dir =oa[0].toString();
		String name =oa[1].toString();
		if (name.equals("2705")) { System.out.println(name+"!!");}
		if (dir.contains("__unicode")) {return null;}
		if (name.contains("__unicode")) {return null;}
		if (name.equals("2705")) { System.exit(1);}
		String className=generateEr_01.firstUpper(name);
		String packagePath = generateEr_01.derivePackage(generateEr_01.cGen+dir);
		//isJavaKeyword = new File ("D:\\_2022\\_2022_04\\tag\\java\\keyword\\"+name).exists();
		//isJavaKeyword = isJavaKeyword || new File ("D:\\_2022\\_2022_04\\tag\\java\\datatype\\basic\\"+name).exists();
		packagePath= packagePath.substring(0, packagePath.length()-1);
		pff.setFileName(generateEr_01.genPath+
				        generateEr_01.cGen+
				        generateEr_01.cPathDelim+dir+"Set"+className+
				        generateEr_01.cInterfPostfix+
				        generateEr_01.javaExt);
		pff.init();
		pff.println(generateEr_01.cPackage+packagePath+generateEr_01.cSemicolon);
		//pff.println("import gen.language.en_2.verb._1.v.Visit_enI;");
		pff.println(generateEr_01.cPublic+generateEr_01.cInterface+generateEr_01.cSpace+"Set"+className
				+generateEr_01.cInterfPostfix+generateEr_01.cSpace+generateEr_01.cBrack_OT_Open_OT_curly);
		//generateTag(packagePath,name);	
		generateMethod(name,className);
		pff.println(generateEr_01.cBrack_OT_Close_OT_curly);
		pff.finish();
		counter++;
		return null;
	}
	
	private void generateMethod(String name, String className) {

		pff.println(generateEr_01.cPublic+generateEr_01.cSpace+"Object"+" "+"set"+className+generateEr_01.cInterfPostfix
				+generateEr_01.cBrack_OT_Open+generateEr_01.firstUpper(name)+generateEr_01.cInterfPostfix+" "+name+generateEr_01.cInterfPostfix+generateEr_01.cBrack_OT_Close+" ; // (Object ... oa) ;");
		//generateEr_01.cVisit_enI
	}


	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return this;
	}
	
}
