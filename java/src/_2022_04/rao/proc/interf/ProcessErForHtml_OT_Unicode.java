package _2022_04.rao.proc.interf;

import java.util.Vector;

import _0001.PrintEr;
import _0001.file.er.p.PrintErForFile;
import _2022_04.rao.GenerateEr_01;
import gen.language.en.verb._1.a.Add_enI;
import gen.language.en.verb._1.e.Execute_enI;
import gen.language.en.verb._1.f.Find_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;
import _2022_04.rao.proc.Processor_OT_All_OT_Vector;

public class ProcessErForHtml_OT_Unicode  //extends PrintEr 
	implements InitProcFinish_enI , Process_enI ,Init_enI,Add_enI,Find_enI,Execute_enI {
	String tag="joker";
	String unicodeForTag="1F0CF";
	String cPng=".png";
	String cHtml=".html";
	String fileIntro="file:///";
	String titleInHtml=" title=\"";
	private String cApo="\"";
	String prefixForHtml= "<a href="+cApo+"test2.html"+cApo+"><img src="+cApo;
	String styleInHtml=" style=\"width:72px;height:72px;\"> </a>";
	String path="D:\\workspace\\interfgen-master\\anchor\\tmp\\html\\";
	String pathForUnicode="file:///D:\\workspace\\interfgen-master\\anchor\\framework\\res\\openmoji-72x72-color\\";
	PrintErForFile pff = new PrintErForFile();
	Vector<TagAndCode> vecForUnicode = new Vector<TagAndCode>();
	
	private GenerateEr_01 generateEr_01;
	private ProcessErForHtml_OT_Unicode r=this;
	private Process_enI process_enI;
	private String line;
	
	public static void main( String args[]){
		ProcessErForHtml_OT_Unicode phu= new ProcessErForHtml_OT_Unicode();
		phu.init();
		//phu.process2(phu.tag,phu.unicodeForTag);
		TagAndCode tc=(TagAndCode) phu.find("taxi");
		System.out.println(tc.code);
		phu.execute();
	}
	
	@Override
	public Object execute(Object... oa) {
		ProcessorForFile procFF= new ProcessorForFile();
		new Processor_OT_All_OT_Vector().process(vecForUnicode,this,procFF);
		pff.setFileName(path+"single\\"+"_total"+cHtml);
		pff.init();
		new Processor_OT_All_OT_Vector().process(vecForUnicode,this,new ProcessorForFile_OT_total());
		pff.finish();
		return null;
	}
	
	@Override
	public Object process(Object... oa) { //2(String tag, String unicodeForTag) {
		TagAndCode tc=(TagAndCode) oa[0];
		//System.out.println("tac process:"+tc.tag);
		Object[] oa2= {"",tc};
		String line=makeHtml(tc);
		if (tc.code.contains(",")) {
			String [] sa =tc.code.split(",");
			line="";
			for (int i=0;i<sa.length;i++) {
			TagAndCode tc2 = new TagAndCode(tag, sa[i]);
			line= line + makeHtml(tc2);
			}
		}
		oa2[0]=line;
		oa2[1]=tc;
		return oa2;

	}
	private String makeHtml(TagAndCode tc) {
		String line=prefixForHtml+pathForUnicode+tc.code+cPng+cApo+titleInHtml+tc.tag+cApo+styleInHtml;
		return line;
	}

	public Object setGenerateEr_01(GenerateEr_01 generateEr_01) {
		this.generateEr_01 = generateEr_01;
		return this;
	}

	/*
	@Override
	public Object process(Object... oa) {
		String dir =oa[0].toString();
		String name =oa[1].toString();
		println("<h4>"+name+"</h4>");
		return null;
	}
*/
	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}
	
	class TagAndCode {
		public TagAndCode(String tag, String code) {
			this.tag=tag;
			this.code=code;
		}
		String tag;
		String code;
	}
	
	@Override
	public Object init(Object... oa) {
        r.add("accuracy","1F4CF");
        r.add("alternative","23FD");
        r.add("ambulance","1F691");
        r.add("angle","1F4D0");
        r.add("animal","1F43E");
        r.add("approach","2194");
        r.add("batteryPower","1FAAB");
        r.add("bicycle","1F6B4");
        r.add("book","1F4DA");
        r.add("box_OT_black","26AB,1F4E6");
        r.add("brake","23EA");
        r.add("briefs","1F463");
        r.add("bus","1F68C");
        r.add("camera","1F4F7");
        r.add("candy","1F36C");
        r.add("car","1F697");
        r.add("check","2705");
        r.add("chemical","2697");
        r.add("child","1F9D2");
        r.add("cityscape","1F3D9");
        r.add("collaboration","E249");
        r.add("collision","1F4A5");
        r.add("compose","E262");
        r.add("construction","1F6A7");
        r.add("create","2795");
        r.add("crossing","274C");
        r.add("cyclone","1F300");
        r.add("delete","E262");
        r.add("doctor","E301");
        r.add("dot","25AA");
        r.add("electrical","26A1"); 
        r.add("end","1F51A");
        r.add("envelope","1F526");
        r.add("explosive","1F9E8");
        r.add("fast","1F407");
        r.add("fire","1F525");
        r.add("fireEngine","1FA84");
        r.add("flashlight","1F526");
        r.add("flood","E09D");
        r.add("heart","2764");
        r.add("heart_OT_Anatomical","1FAC0");
        r.add("helicopter","1F681");
        r.add("hole","1F573");
        r.add("ice","1F9CA");
        r.add("infectious","2623");
        r.add("java","1F463,1F9BA");
        r.add("joker","1F0CF");
        r.add("key","1F511");
        r.add("landslide","E09D");
        r.add("light","1F4A1");
        r.add("lightBulp","1F4A1");
        r.add("liquid_OT_pouring","1F4F7");
        r.add("location","E242");
        r.add("medical","E309");
        r.add("mirror","1FA9E");
        r.add("motorcycle","1F3CD");
        r.add("new","1F195");
        r.add("object","1FA84");
        r.add("on","1F51B");
        r.add("open","1F450");
        r.add("OpenStreetMap","openstreetmap");
        r.add("pedestrian","1F6B6");
        r.add("personBlind","1F9D1-200D-1F9AF");
        r.add("play","25B6");
     	
        r.add("police","1F693");
        r.add("postbox","1F4EE");
        r.add("predict","1F51C");
        r.add("problem","1F6A9");
        r.add("protected","1F510");
        r.add("question","2754");
        r.add("radioactive","2622");
        r.add("rain","1F327");
        r.add("record","23FA");
        r.add("reflect","1FA9E");
        r.add("remind","1F397");
        r.add("reverse","25C0");
        r.add("roadwork","26D1");
        r.add("robot","1F916");
        r.add("rock","1FAA8");
        r.add("safety","E247");
        r.add("safetyPin","1F9F7");
        r.add("safetyVest","1F9BA");
        r.add("scale","1F9D2");
        r.add("send","1F4EE");
        r.add("ship","1F6A2");
        r.add("signForStop","1F6D1");
        r.add("slow","1F40C");
        r.add("snow","2744");
        r.add("snowflake","2744");
        r.add("start","1F3AC");
        r.add("stopForBus","1F68F");
        r.add("structure","1F9EC");
        r.add("taxi","1F695");
        r.add("temperature","1F321");
        r.add("thermometer","1F321");
        r.add("tornado","1F32A");
        r.add("traffic","1F69B,1F697,1F6B6");
        r.add("trafficLight","1F6A6");
        r.add("train","1F686");
        r.add("tram","1F68A");
        r.add("transport","1F686,1F68C,1F6A2");
        r.add("transport_OT_public","1F450,1F686,1F68C,1F6A2");
        r.add("tree","1F332");
        r.add("truck","1F69B");
        r.add("violation","2694");
        r.add("volcano","1F30B");
        r.add("wait","23F3");
        r.add("warning","270B");
        r.add("weather","2600,1F327,2744");
        r.add("wheel","1F6DE");
        r.add("wikidata","E04B");
        r.add("zoom","1F50D");

		

		return null;
	}
	@Override
	public Object add(Object... oa) {
		String tag=(String) oa[0];
		String code=(String) oa[1];
		vecForUnicode.add(new TagAndCode(tag,code));
		return null;
	}
	@Override
	public Object find(Object... oa) {
		String tag=(String) oa[0];
		TagAndCode tc =null;
		for (int i=0;i<vecForUnicode.size();i++) {
			if (tag.equals(vecForUnicode.get(i).tag)) {
				tc=vecForUnicode.get(i);
				return tc;
			}
		}
		return null;
	}
	class ProcessorForFile_OT_total implements Process_enI {
		@Override
		public Object process(Object... oa) {
			String line=(String) oa[0];
			TagAndCode tc=(TagAndCode) oa[1];
			System.out.println("total process:"+line);
			pff.println(line+"<br>");
			pff.println("<h4>"+tc.tag+"<h4>");
			return null;
		}
	}
	class ProcessorForFile implements Process_enI {

		@Override
		public Object process(Object... oa) {
			String line=(String) oa[0];
			TagAndCode tc=(TagAndCode) oa[1];
			System.out.println("file process:"+line);
			pff.setFileName(path+"single\\"+tc.tag+cHtml);
			pff.init();
			// fileIntro+path
			pff.println(line);
			pff.finish();
			return null;
		}
		
	}


}
