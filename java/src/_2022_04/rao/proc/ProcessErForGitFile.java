package _2022_04.rao.proc;

import _0001.PrintEr;
import _0001.file.er.p.PrintErForFile;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;

public class ProcessErForGitFile extends PrintEr implements Process_enI,Init_enI {
PrintErForFile pff = new PrintErForFile();

private String cAnchor="anchor";

String rootPath= "D:\\workspace\\interfgen-master\\"+cAnchor;

public static void main( String args[]){
	new ProcessErForGitFile().process("dir","\\java");
}
	@Override
	public Object process(Object... oa) {
		String fileType=oa[0].toString();
		String name=oa[1].toString();
		println("process!"+fileType+" "+name);
		if (fileType.equals("dir")) {
			pff.setFileName(rootPath+name+"\\.gitkeep");
			pff.init();
			pff.finish();
		}
		return null;
	}

	@Override
	public Object init(Object... oa) {
		
		return null;
	}

}
