package _2022_04.rao;

import java.io.File;
import java.util.Vector;



//import org.zackify.en._000.interf.p.InitProcFinish_enI;
/*
import org.zackify.en._000.interf.v.Visit_enI;
*/
import _0001.file.er.r.ReadErForFile;
import _2022_04.rao.proc.ProcessErForGitFile;

//import gen.language.en.other.u.Unicode_enI;

import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.language.en.verb._1.p.SetProcess_enI;
import gen.language.en.verb._1.v.Visit_enI;

//import gen.language.en.verb._2.e.Exclude_enI;
import gen.programming.code.java.zack.InitProcFinish_enI;

//import gen.programming.code.java.InitProcFinish_enI;



public class ProcessErForFolder implements InitProcFinish_enI,Visit_enI,Process_enI,SetProcess_enI,
Init_enI,Finish_enI {
	// flag if in unicode dir
	public boolean unicodeDir=false;
	


	public boolean isUnicodeDir() {
		return unicodeDir;
	}

	public void setUnicodeDir(boolean unicodeDir) {
		this.unicodeDir = unicodeDir;
	}
	
	Process_enI process_enI = null ; //new ProcessErForGitFile();
	
	public static void main( String args[]){
	ReadErForFile rff = new ReadErForFile();
	ProcessErForFolder pff = new ProcessErForFolder();
	pff.visit();
	}
	/*
	 * 		try {
			set = listFilesUsingFileWalkAndVisitor(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	 */
	public String rootPath;
	Vector v = new Vector();
	protected boolean debug=false;
	public String cPathDelim = "\\";
	private String cAnchor="anchor";
	boolean isOverride=false; //true; // as for git processor
	
	

	//---------------------------------------------
	@Override
	public Object visit(Object ... oa) {
		if (process_enI!=null) ((Init_enI) process_enI).init();
		init();
		v.add("tag");
		//rootPath= "D:\\_2022\\_2022_04\\"+cAnchor;
		rootPath= "D:\\workspace\\interfgen-master\\"+cAnchor;
		listDir(0,rootPath);
		finish();
		return this;
	}
	
	//---------------------------------------------
	public void listDir(int level,String dirName) {
    File fileName = new File(dirName);
    //if (level==0) process(fileName); 
    File[] fileList = fileName.listFiles();
    
    for (File file: fileList) { 

		if (noExclude(file.getAbsolutePath())||isOverride) {
			processFile(file, file.getAbsolutePath());
			if (file.isDirectory()) {
				listDir(level + 1, file.getAbsolutePath());
			} else {
				processFile(file, file.getAbsolutePath());
			}
		}
		setUnicodeDir(false);
    }
	}
	
	//---------------------------------------------
	private boolean noExclude(String fn) {
		String excludeName =fn+cPathDelim+"__"+"exclude" ; //Exclude_enI.t;//;
		String unicodeName =fn+cPathDelim+"__"+"unicode";// Unicode_enI.t;//  
		//if ( new File(unicodeName).exists() ) { setUnicodeDir(true); };
		/*
		if ((fn+cPathDelim).contains("__"+"unicode")) { 
			setUnicodeDir(true); 
			System.out.println(fn+cPathDelim);
		};
		*/
		//System.out.println("exc:"+excludeName+ " "+new File(excludeName ).exists());
		if (   new File(excludeName ).exists() // || new File(unicodeName ).exists()
			) { 
			// System.out.println("exclude!");
			return false; }
		return true;
	}

	/*
	public Set<String> listFilesUsingFileWalkAndVisitor(String dir) throws IOException {
	    final Set<String> fileList = new HashSet<>();
	    Files.walkFileTree(Paths.get(dir), new SimpleFileVisitor<Path>() {
	    	public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
	    		 fileList.add("initDir_"+dir.getFileName().toString());
	    		 return FileVisitResult.CONTINUE;
	    	}
	    	public FileVisitResult postVisitDirectory(Path dir, BasicFileAttributes attrs) {
	    		fileList.add("finishDir_"+dir.getFileName().toString());
	    		return FileVisitResult.CONTINUE;
	    	}
	    	
	        @Override
	        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
	          throws IOException {
	            if (!Files.isDirectory(file)) {
	                fileList.add(file.getFileName().toString());
	            }
	            return FileVisitResult.CONTINUE;
	        }
	    });
	    return fileList;
	}
	*/
	public  void processFile(File f ,String s) {
		String dir="file" ;
		if (f.isDirectory()) dir="dir";
		//s=s.replace(rootPath,"");
		//System.out.println("r1:"+rootPath+" "+dir);
		s=s.replace(rootPath, "");
		//System.out.println("r2:"+rootPath+" "+dir);
		process(dir,s); 
	}




	@Override
	public Object process(Object... oa) {
		if (process_enI!=null) {
			process_enI.process(oa);
		} else {
		String dir = oa[0].toString();
		String s = oa[1].toString();
		if (true) {System.out.println("process:"+dir+" "+s); }
		}
		return this;
	}

	@Override
	public Object init(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object finish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}


}
