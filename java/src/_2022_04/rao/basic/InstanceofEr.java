package _2022_04.rao.basic;

import gen.java.keyword.Instanceof_enI;
import gen2.language.en.verb._1.v.Visit_enI;

public class InstanceofEr extends VisitEr implements Instanceof_enI {
	
	public static void main( String args[]){
		Visit_enI visit_enI = new VisitEr();
		InstanceofEr r= new InstanceofEr();
		System.out.println(r.instanceof_(visit_enI,r));
	}
	
	@Override
	public Object instanceof_(Object... oa) {
		Class c = oa[1].getClass();	
		return  Boolean.valueOf(c.isInstance (oa[0] ));
	}

}
