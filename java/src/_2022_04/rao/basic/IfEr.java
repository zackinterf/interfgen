package _2022_04.rao.basic;

import java.io.File;
import java.util.Date;

import org.zackify.en.prep.er.p.PrintEr;

import gen.java.keyword.If_enI;
import gen2.language.en.verb._1.p.Print_enI;
import gen2.language.en.verb._1.p.SetPrint_enI;
import gen2.language.en.verb._1.v.Visit_enI;

public class IfEr implements If_enI,Visit_enI,SetPrint_enI {
	
	public static void main( String args[]){
		//new File().
		//new Date().
		//new Thread().
		Object o1=Boolean.FALSE;
		VisitEr visitEr = new VisitEr();
		IfEr r = new IfEr();
		 ((If_enI) r.setPrint_enI(new PrintEr()))
		 .if_(o1,r,visitEr);
	}

	private IfEr ifEr;
	private Print_enI print_enI;
	
	public IfEr getIfEr() {
		return ifEr;
	}

	public void setIfEr(IfEr ifEr) {
		this.ifEr = ifEr;
	}

	public IfEr() {
		this.ifEr = this;
	}
	
	@Override
	public Object if_(Object... oa) {
		if (oa[0] instanceof Boolean) {
			Boolean booleanU = (Boolean) oa[0];
			boolean b=booleanU.booleanValue();
			if (b) {
				visit_OT_mine(oa,1);
			} else {
				if (oa.length>2) {
				  visit_OT_mine(oa,2);
				}
			}
		}
		return null;
	}

	private void visit_OT_mine(Object[] oa, int i) {
		if (oa[i] instanceof Visit_enI) {
			Visit_enI visit_enI = (Visit_enI) oa[i];
			visit_enI.visit();
		} else {
		}
		
	}

	@Override
	public Object visit(Object... oa) {
		System.out.println("hallo!");
		return this;
	}

	public  Object setPrint_enI(Print_enI print_enI) {
		this.print_enI = print_enI;
		return this;
	}

	@Override
	public Object print(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}



}
