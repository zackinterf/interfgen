package _2022_04.rao.basic;

import gen.programming.code.java.zack.InitProcFinish_enI;
import gen2.language.en.verb._1.f.Finish_enI;
import gen2.language.en.verb._1.f.SetFinish_enI;
import gen2.language.en.verb._1.i.Init_enI;
import gen2.language.en.verb._1.i.SetInit_enI;
import gen2.language.en.verb._1.p.Process_enI;
import gen2.language.en.verb._1.p.SetProcess_enI;
import gen2.language.en.verb._1.v.SetVisit_enI;

public class InitProcFinishErOld 
implements InitProcFinish_enI, 
SetInit_enI,
SetProcess_enI,
SetFinish_enI,
SetVisit_enI
{

	private Init_enI init_enI;
	private Process_enI process_enI; 
	private Finish_enI finish_enI;

	@Override
	public Object init(Object... oa) {
		if (init_enI!=null) { init_enI.init(oa);}
		return this;
	}

	@Override
	public Object process(Object... oa) {
		if (process_enI!=null) { process_enI.process(oa);}
		return this;
	}

	@Override
	public Object finish(Object... oa) {
		if (finish_enI!=null) { finish_enI.finish(oa);}
		return this;
	}

	@Override
	public Object visit(Object... oa) {
		init(oa);
		process(oa);
		return finish(oa);
	}
/*
	@Override
	public Object setVisit_enI(gen.language.en_2.verb._1.v.Visit_enI visit_enI) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object setFinish_enI(Finish_enI finish_enI) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object setProcess_enI(Process_enI process_enI) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object setInit_enI(Init_enI init_enI) {
		// TODO Auto-generated method stub
		return null;
	}

*/

	@Override
	public Object initProcFinish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

}
