package _2022_04.rao.basic;

import java.util.Vector;

import org.zackify.en.prep.er.p.PrintEr;

import gen.language.en.verb._1.a.Add_enI;
import gen.language.en.verb._1.c.Contains_enI;
import gen.language.en.verb._1.p.Process_OT_all_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.language.en.verb._1.p.SetProcess_enI;

public class VectorEr // extends PrintEr
implements Add_enI,Contains_enI, Process_OT_all_enI //Process_enI,SetProcess_enI 
{

public Vector<Object> v = new Vector();
Process_enI process_enI;

boolean onlyNew=true;

public static void main( String args[]){
	
	VectorEr vectorEr = new VectorEr();
	vectorEr.add("test");
	vectorEr.add("test");
	System.out.println(""+vectorEr.contains("test")+vectorEr.v.size());
}
@Override
public Object add(Object... oa) {
	if (onlyNew) {
		if (!v.contains(oa[0])) {
			return (Object) v.add(oa[0]);
		} else
			return this;
	}
	return (Object) v.add(oa[0]);
}

@Override
public Object contains(Object... oa) {
	return v.contains(oa[0]);
}

public Object process(Object... oa) {
	Integer iU = (Integer)oa[0];
	System.out.println(iU.intValue()+" "+oa[1]);
	return null;
}
@Override
public Object process_OT_all(Object... oa) {
	for (int i=0;i<v.size();i++) {
		process(Integer.valueOf(i),v.get(i));
	}
	return null;
}
}
