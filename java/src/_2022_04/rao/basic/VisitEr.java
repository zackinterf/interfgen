package _2022_04.rao.basic;

import gen.language.en.verb._1.v.SetVisit_enI;
import gen.language.en.verb._1.v.Visit_enI;

public class VisitEr implements Visit_enI,SetVisit_enI {
	
	private Object o;
	private Visit_enI visit_enI;
	private CheckErForParams checkErForParams; 



	public static void main( String args[]){
		new VisitEr().visit();
	}
	//==========================

	
	public Visit_enI setCheckErForParams(CheckErForParams checkErForParams) {
		this.checkErForParams = checkErForParams;
		return this;
	}
	
	public CheckErForParams getCheckErForParams() {
		return checkErForParams;
	}
	
	@Override
	public Object visit(Object... oa) {
		// checkErForParams.check(oa);
		if (visit_enI!=null) { 
			   return  visit_enI.visit(); 
			} else 
				System.out.println("x");
		return this;
	}




}
