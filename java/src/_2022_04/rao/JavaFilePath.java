package _2022_04.rao;

import java.io.File;
import java.io.IOException;

public class JavaFilePath {

	public static void main(String[] args) throws IOException {
		File file = new File("D:\\_2022\\_2022_04\\test.txt");
		printPaths(file);


	}

	private static void printPaths(File file) throws IOException {
		System.out.println("Absolute Path: " + file.getAbsolutePath());
		System.out.println("Canonical Path: " + file.getCanonicalPath());
		System.out.println("Path: " + file.getPath());
	}

}
