package _0001.file.er.p;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.p.Print_enI;
import gen.language.en.verb._1.p.Println_enI;
import gen.language.en.verb._1.p.Process_enI;
import gen.language.en.verb._1.v.Visit_enI;
/*
import org.zackify.en._000.interf.e.Execute_enI;
import org.zackify.en._000.interf.f.Finish_enI;
import org.zackify.en._000.interf.i.Init_enI;
import org.zackify.en._000.interf.p.Print_enI;
import org.zackify.en._000.interf.p.Println_enI;
import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;
import org.zackify.en.interf.i.Init_OT_before_enI;

import _000.core.er.i.InitExecFinishEr;
*/

public class PrintErForFile //extends InitExecFinishEr 
implements Init_enI,Finish_enI,
Process_enI,Print_enI,Println_enI {
	String        fileName  =        null;
	public String getFileName() {
		return fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	String        line  =        "";
	java.io.BufferedWriter        bufferedWriter  =        null;
	java.io.FileWriter        fileWriter  =        null;
	PrintErForFile pf;
	
	public static void main(String[] args) {
		PrintErForFile pf = new PrintErForFile();
		pf.pf=pf;
		pf.init_OT_before();
		pf.init();
		pf.process("test");
		pf.finish();
	}
	
	
	@Override
	public  Object finish (Object... oa) {
		try {	
			bufferedWriter.close();
			fileWriter.close();
			} catch (Exception e) { e.printStackTrace(); }
			 return this; 
	}

	@Override
	public  Object init (Object... oa) {
		try {
			fileWriter= new FileWriter(fileName);
			bufferedWriter = new BufferedWriter(fileWriter);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}



	public void init_OT_before() {
		fileName="d:/x/test.txt";
		line="test";
	}

/*
	@Override
	public Visit_enI visit() {
		pf.init_OT_before();
		pf.init();
		pf.execute();
		pf.finish();

		return null;
	}
*/

	@Override
	public Visit_enI process(Object ... oa) {
		try {
			bufferedWriter.write( (String) oa[0].toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public Visit_enI print(Object ... oa) {
		return process(oa[0]);
	}


	@Override
	public Visit_enI println(Object ... oa) {    
		return print(oa[0].toString()+"\n");
	}
}
