package _0001.file.er.r;

import java.io.*;

import _0001.InitExecFinishEr;
import gen.language.en.verb._1.p.Process_enI;
/*
import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;

import _000.core.er.i.InitExecFinishEr;

*/

public class ReadErForFile extends InitExecFinishEr 
implements Process_enI {
	Process_enI processO1_enI = this;
	int l;


	public static void main( String args[]){
		ReadErForFile rff = new ReadErForFile();
		//rff.setArgs(args).visit(); 
		rff.init();
		rff.execute();
		System.out.println(rff.line);
		rff.finish();
		}

	public int getL() {
	return l;
}
public void setL(int l) {
	this.l = l;
}
	public Process_enI getProcessO1_enI() {
		return processO1_enI;
	}
	public void setProcessO1_enI(Process_enI processO1_enI) {
		this.processO1_enI = processO1_enI;
	}
	String fileName="d:\\temp.txt";
	String line;
	int maxlines=-1;
	FileReader fileReader;
	BufferedReader bufferedReader;
	public boolean logIt=true;
	
	public FileReader getFileReader() {
		return fileReader;
	}
	public void setFileReader(FileReader fileReader) {
		this.fileReader = fileReader;
	}
	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}
	public void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	
 

//--------------------------
public void initOTbefore() {
if (isLogIt()) System.out.println("//initOTbefore"); 

try {
//if (getInitOTadditional_enI()!=null){
//	getInitOTadditional_enI().initOTadditional();
//	}
} catch (Exception e) { e.printStackTrace(); }
 }

private boolean isLogIt() {
	// TODO Auto-generated method stub
	return logIt;//false;
}
//--------------------------
public  Object init (Object... oa) {
	//setProcessO1_enI(this);
if (isLogIt()) System.out.println("//init"); 

try {
setFileReader(new FileReader(getFileName()));
setBufferedReader(new BufferedReader(getFileReader()));
} catch (Exception e) { e.printStackTrace(); }
return this; }
//--------------------------
public  Object finish (Object... oa) {

if (isLogIt()) System.out.println("//finish"); 

try {
if (getFileReader()!=null ) {getFileReader().close();}
if (getBufferedReader()!=null) {getBufferedReader().close();}
} catch (Exception e) { e.printStackTrace(); }
return this; }
//--------------------------
public  Object execute (Object... oa) {
	if (isLogIt()) System.out.println("//execute"); 

try {	
if 	(getBufferedReader()!=null) {
setLine(getBufferedReader().readLine()); //println(getLine());
setL(getL()+1);
if (getProcessO1_enI()!=null){
	getProcessO1_enI().process(getLine());
}
while (getLine()!= null && (maxlines==-1 ||getL()<maxlines ) ){
	setLine(getBufferedReader().readLine());
	setL(getL()+1);
	if (getProcessO1_enI()!=null){
		if (getLine()!= null) {
			getProcessO1_enI().process(getLine());
		}
	}
}
} // if null
} catch (Exception e) { e.printStackTrace(); }
return this; }
@Override
public Object process(Object ... oa) {
	System.out.println(oa);
	return this;
}

}
//finish