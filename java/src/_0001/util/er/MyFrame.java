package _0001.util.er;

import javax.swing.JFrame;

import _0001.db.er.DbHandlerOT_hsql;



public class MyFrame extends JFrame {
    public MyFrame mf=this;
    public DbHandlerOT_hsql dbh = new DbHandlerOT_hsql();
    
	public MyFrame() {
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
            	mf.dbh.finish();
                System.exit(0);
            }
        });
    }

}
