package _0001.util.er;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.zackify.en._000.interf.e.Execute_enI;
import org.zackify.en._000.interf.p.Process_enI;
import org.zackify.en._000.interf.v.Visit_enI;

public class CallErForURL implements Execute_enI,Process_enI{
	String webpage="https://www.zackify.eu:3001/";
	public static void main(String[] args) {
		new CallErForURL().execute();
	}
	@Override
	public Visit_enI execute() {
		process("tags/l/fini$p");
		return null;
	}
	@Override
	public Visit_enI process(Object o) {
		String inputLine="";
		try {
		URL url = new URL(webpage+o.toString());
		URLConnection urlC = url.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                    urlC.getInputStream()));

        while ((inputLine = in.readLine()) != null) 
            System.out.println(inputLine);
        in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
