package _0001.util.er;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import java.util.Random;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JFrame;

//import org.apache.xalan.templates.ElemValueOf;

import gen.language.en.verb._1.i.Init_enI;


public class DrawErForText extends JComponent implements Init_enI {

	Random random = new Random();
	JFrame frame = new JFrame("Draw Line");
	Vector<String> vecForText = new Vector<String>();
	Vector<Boolean> vecForB = new Vector<Boolean>();
	private int maxI=400;
    
	
	public void paint(Graphics g) {
		int lastx=10;
		int lasty=10;
		int xOff=100;
		int yOff=100;
		resetBool();
		for (int i=0;i<4;i++) {
			
			int x=getRI(maxI);
			int y=getRI(maxI);
			g.setColor(new Color(getRI(255),getRI(255),getRI(255)));

			g.drawLine(lastx, lasty, x+xOff, y+yOff);
			lastx=x+xOff;
			lasty=y+yOff;
		g.setColor(new Color(getRI(255),getRI(255),getRI(255)));
		g.drawString(getRT(), x+xOff, y+yOff);
		}
		/*
		 * Graphics2D g2 = (Graphics2D) g; g2.setStroke(new BasicStroke(2f));
		 * g2.setColor(Color.RED); g2.draw(new Line2D.Double(50, 150, 250, 350));
		 */
	}

	private String getRT() {
		int r=getRI(vecForText.size());
        while (vecForB.get(r).booleanValue()) { 
		  r=getRI(vecForText.size());
        }
        Boolean b = vecForB.get(r);
        b.parseBoolean("true");
		return vecForText.get(r);
	}

	private int getRI(int max) {
		return random.nextInt(max);
	}

	public static void main(String[] args) {
		DrawErForText d = new DrawErForText();
		d.init();
	}


	private void resetBool() {
		for (int i=0;i<vecForB.size();i++) {
			Boolean b = vecForB.get(i);
			b = Boolean.parseBoolean("false");
		}
	}
	private void initVec() {
		add("test");
		add("re");
		add("wild");
		add("science");
		add("system");
		add("dim");
		add("mass");
		add("energy");
		add("loc");
		add("size");
		add("form");
		add("lang");
	}

	private void add(String string) {
		vecForText.add(string);
		vecForB.add(Boolean.valueOf(false));
	}
/*
	public Visit_enI init2() {
		JFrame frame = new JFrame("Hello");
		frame.getContentPane().add(this);
		this.setSize(300, 300);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		// frame.pack();
		return null;
	}
*/
	@Override
	public Object init(Object... oa) {
		initVec();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(this);
		frame.pack();
		frame.setSize(new Dimension(1000, 900));
		frame.setVisible(true);
		while (true) {
			try {
				Thread.sleep(1800);
				frame.revalidate();
				frame.repaint();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//return null;
	}
}