package _0001;

import gen.language.en.verb._1.e.Execute_enI;
import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.v.Visit_enI;

/*
import org.zackify.en._000.interf.e.Execute_enI;
import org.zackify.en._000.interf.f.Finish_enI;
import org.zackify.en._000.interf.g.GetP_enI;
import org.zackify.en._000.interf.i.Init_enI;
import org.zackify.en._000.interf.s.SetArgsSa_enI;
import org.zackify.en._000.interf.v.Visit_enI;

import _000.core.er.m.MinEr;
import _000.core.er.p.PrintEr;
import _000.core.er.t.This_enI;
*/


public class InitExecFinishEr  implements Visit_enI,Init_enI,Execute_enI,Finish_enI
// implements Visit_enI,Init_enI,Execute_enI,Finish_enI,GetP_enI,SetArgsSa_enI,This_enI 
{

	@Override
	public Object visit(Object... oa) {
		init();
		execute();
		finish();
		return null;
	}
/*
	PrintEr printEr = new PrintEr();

	public MinEr getMinEr() {
		return printEr.getMinEr();
	}
	
	@Override
	public PrintEr getP() {
		return printEr;
	}
	
    private String[] args;

	public String[] getArgs() {
		return args;
	}
	
	@Override
	public Visit_enI setArgs(String[] args) {
		this.args=args;
		return this;
	}
	
	@Override
	public Object visit(Object ... oa) {
		init();
		execute();
		finish();
		return getMinEr().getVisit_enI();
	}


	@Override
		public Object init(Object... oa) {
		return printEr.getMinEr().getVisit_enI();
	}

	@Override
	public Visit_enI execute() {
		return printEr.getMinEr().getVisit_enI();
	}

	@Override
	public Object finish(Object... oa) {
		return printEr.getMinEr().getVisit_enI();
	}

	@Override
	public Visit_enI this_() {
		return this;
	}

*/

	@Override
	public Object finish(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object execute(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object init(Object... oa) {
		// TODO Auto-generated method stub
		return null;
	}


}
