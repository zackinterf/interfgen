package _0001;

import gen.language.en.verb._1.p.Print_enI;
import gen.language.en.verb._1.p.Println_enI;
import gen.language.en.verb._1.p.SetPrint_enI;

public class PrintEr implements Print_enI,Println_enI,SetPrint_enI {

	@Override
	public Object println(Object... oa) {
		print(oa);
		print("\n");
		return this;
	}

	@Override
	public Object print(Object... oa) {
		System.out.print(oa[0]);
		return this;
	}

}