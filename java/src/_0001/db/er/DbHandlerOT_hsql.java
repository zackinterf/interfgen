package _0001.db.er;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import gen.java.keyword.This_enI;
import gen.language.en.verb._1.e.Execute_enI;
import gen.language.en.verb._1.f.Finish_enI;
import gen.language.en.verb._1.i.Init_enI;
import gen.language.en.verb._1.v.Visit_enI;


/*
import _000.core.er.i.InitExecFinishEr;
import org.zackify.en._000.interf.e.Execute_enI;
import org.zackify.en._000.interf.f.Finish_enI;
import org.zackify.en._000.interf.i.Init_enI;
import org.zackify.en._000.interf.v.Visit_OT_all_enI;
import org.zackify.en._000.interf.v.Visit_enI;
*/

public class DbHandlerOT_hsql //extends InitExecFinishEr
implements Init_enI,Finish_enI,Execute_enI,This_enI
//,Visit_OT_all_enI
{
Connection connection;
ResultSet resultSet;
Statement statement;
private String dBName="raoul_2017_04";
private String query="select * from tag_link_en where source like 'center%'";
boolean list=true;

public static void main(String[] args) {
	DbHandlerOT_hsql dbh = new DbHandlerOT_hsql();
	dbh.init();
	dbh.execute();
	dbh.finish();
}
public String getQuery() {
	return query;
}
public void setQuery(String query) {
	this.query = query;
}

@Override
public  Object init (Object... oa) {

		try {
			Class.forName("org.hsqldb.jdbc.JDBCDriver");
		connection=
    	   // DriverManager.getConnection("jdbc:hsqldb:file:"+getFilename()+"; shutdown=true", "SA", "superduper" );
    	   //	"jdbc:hsqldb:hsql://localhost/"
    		DriverManager.getConnection(getdBconnectString()+"; shutdown=true", "SA", "superduper" );
		statement= connection.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	return this_();
}
    private String getdBconnectString() {
    	return "jdbc:hsqldb:hsql://localhost/"+getdBName();
    }
    
private String getdBName() {
		return dBName;
	}
@Override
public Object execute(Object... oa)  {
	try {
		if (query.toLowerCase().startsWith("select") ) {
			statement.execute(getQuery());			
		} else {
			statement.executeUpdate(getQuery());						
		}
	resultSet=statement.getResultSet();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	if (list) {
		//visit_OT_all() ;
	}
	return this_();
}
@Override
public Object finish(Object... oa) {
	try {
		connection.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return this_();
}
/*
@Override
public Visit_enI visit_OT_all() {
	try {
	while (resultSet.next()) {
		getP().println(resultSet.getString(1));
	}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return this_();
}
*/
@Override
public Object this_(Object... oa) {
	// TODO Auto-generated method stub
	return this;
}



}
